package main

import (
    "io"
    "strconv"
    "time"
    "os"
    "fmt"
    "flag"
    "net"
    "log"
    "reflect"
    "net/http"
    "golang.org/x/net/context"

	"github.com/gobwas/ws"
    "github.com/gobwas/ws/wsutil"
    
    "firebase.google.com/go"
    //"firebase.google.com/go/auth"

    "google.golang.org/api/option"
    
    as "github.com/aerospike/aerospike-client-go"

    "github.com/json-iterator/go"

    xmpp "github.com/mattn/go-xmpp"
    "github.com/satori/go.uuid"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary
var firebaseApp *firebase.App
var asClient *as.Client
var connMeshMap = make(map[string]*net.Conn)
var connClientMap = make(map[string]*net.Conn)
var Policy = as.NewPolicy()
var WritePolicy = as.NewWritePolicy(0, 0)
var ipAddr string = mustHardwareAddr()
var port string
var orderedMapPolicy = as.NewMapPolicy(as.MapOrder.KEY_ORDERED, as.MapWriteMode.UPDATE)

type jMap map[string]interface{}

var fcmClient *xmpp.Client = nil
var fcmServer string = "fcm-xmpp.googleapis.com:5236"
var fcmUser string = "626577548385@gcm.googleapis.com"
var fcmPassword string = "AAAAkeLupGE:APA91bFEheYWnWHzOtSxeZ4W0kR24WCWRi-pYPTls65W20D5VoZ_9yXcV4yzCZAId-ClXjjhPuCbYCHctUzcaMMR5GUIwykmSR9lq0X2Y3g8XKs5iqpivlgq8-Bvnhl0yd_MV0QkaYNu"
 
func insertMsgIntoAerospike(dat *map[string]interface{}, msg *[]byte) {
    fmt.Println("##########$$$$$$$$$$$$$111111")
    from := (*dat)["from"].(string)
    to := (*dat)["to"].(string)
    timestamp := int64((*dat)["timestamp"].(float64))
    var connId string
    if from < to {
        connId = from + ":" + to
    } else {
        connId = to + ":" + from
    }
    key, err := as.NewKey("mesh", "chats", connId)
    cdtMapPolicy := as.NewMapPolicy(as.MapOrder.KEY_ORDERED, as.MapWriteMode.UPDATE)
    pmap := map[interface{}]interface{}{
        timestamp: *msg,
    }

    _, err = asClient.Operate(WritePolicy, key,
        as.MapPutItemsOp(cdtMapPolicy, "common", pmap),
        as.MapRemoveByKeyRangeOp("common", nil, timestamp - 3600000, as.MapReturnType.NONE),
    );
    if err != nil {
        fmt.Println("##########$$$$$$$$$$$$$", err)
    }
}

func mustHardwareAddr() string {
	return "127.0.0.1"
}

func newXmppConn() {
    for {
        client, err := xmpp.NewClient(fcmServer, fcmUser, fcmPassword, false)
        if err != nil {
            time.Sleep(100 * time.Millisecond)
            continue
        }
        fcmClient = client
        break
    }
}

func xmppInitiate() {
    newXmppConn()
    for {
        stanza, err := fcmClient.Recv()
        if err != nil || stanza == io.EOF {
            newXmppConn()
            continue
        }
        fmt.Println(stanza, err)
        fmt.Println(reflect.TypeOf(stanza), err)
        chat := stanza.(xmpp.Chat)
        fmt.Println(chat.Other[0])
        var msg map[string]interface{}
        result := []byte(chat.Other[0])
        json.Unmarshal(result, &msg)
        if msg["message_type"] == "ack" {
            fmt.Println("ACK:::::::", chat.Other[0])
        } else if msg["message_type"] == "nack" {
            fmt.Println("NACK:::::::", msg["error"])
        }  else if msg["message_type"] == "control" && msg["control_type"] == "CONNECTION_DRAINING" {
            newXmppConn()
        } else {
            fmt.Println("Unknown ack:::::::", chat.Other[0])
        }
    }
}

func sendPush(pushToken *string, msg *[]byte, dat *map[string]interface{}) {
    if (*dat)["msgtype"] == "im" {
        u1 := uuid.Must(uuid.NewV4())
        toMsg := fmt.Sprintf(`
            <message id="">
                <gcm xmlns="google:mobile:data">
                { 
                    "data": {
                        "msg": %s
                    },
                    "message_id": "%s"
                    "to" : "%s"
                }
                </gcm>
            </message>`, strconv.Quote(string((*msg)[:])), u1, *pushToken)
        fcmClient.SendOrg(toMsg)
    }
}

func handleSendMessage(dat *map[string]interface{}, uid *string, msg *[]byte) {
    key, err := as.NewKey("mesh", "data", *uid)

    record, err := asClient.Get(Policy, key, "ip", "pushtoken")
    if err != nil {
        // handle error
        fmt.Println("dattttttttttttt777777777", err)
        return
    }

    if record == nil ||  record.Bins["pushtoken"] == nil {
        return
    }
    pushMap := record.Bins["pushtoken"].(map[interface{}]interface{})
    var ipMap map[interface{}]interface{}
    if record.Bins["ip"] != nil {
        ipMap = record.Bins["ip"].(map[interface{}]interface{})
    }
    fmt.Println("dattttttttttttt88888888", uid, record, ipMap, pushMap)
    for deviceId, pushToken := range pushMap {
        pt := pushToken.(string)
        sendPush(&pt, msg, dat)               // TODO: comment this line, this is for testing
        return
        if record.Bins["ip"] == nil {
            sendPush(&pt, msg, dat)
            continue
        }
        wsAddr := ipMap[deviceId]
        if wsAddr == nil {
            sendPush(&pt, msg, dat)
            continue
        }
        fmt.Println("xxxxxxxxxxxx222222222", deviceId, wsAddr)
        meshConn, ok := connMeshMap[wsAddr.(string)]
        if !ok {
            newConn, _, _, err := ws.DefaultDialer.Dial(context.Background(), wsAddr.(string))
            meshConn = &newConn
            connMeshMap[wsAddr.(string)] = meshConn
            if err != nil {
                meshConn = nil
                sendPush(&pt, msg, dat)
                continue
            }
        }
        if meshConn != nil {
            fmt.Println("%%%%%%%%%%%%%%%%%", *meshConn)
            (*dat)["toDeviceId"] = deviceId
            (*dat)["toUid"] = *uid
            (*dat)["toPushToken"] = pushToken
            msgTo, err := json.Marshal(*dat)
            if err != nil {
                sendPush(&pt, msg, dat)
                continue
            }
            err = wsutil.WriteClientMessage(*meshConn, ws.OpText, msgTo)
            if err != nil {
                sendPush(&pt, msg, dat)
                continue
            }
        } else {
            fmt.Println("dattttttttttttt88888888888")
        }
    }
}

func putPartialUserMapAndGet(dat *map[interface{}]interface{}, uid *string) (*map[string]interface{}) {
    cdtMapPolicy := as.NewMapPolicyWithFlags(as.MapOrder.UNORDERED, as.MapWriteFlagsPartial)
    key, err := as.NewKey("mesh", "userinfo", *uid)
    _, err = asClient.Operate(WritePolicy, key,
        as.MapPutItemsOp(cdtMapPolicy, "common", *dat),
    );
    rec, err := asClient.Get(Policy, key, "common")
    if err == nil && rec != nil {
        profileMap := rec.Bins["common"].(map[interface{}]interface{})
        profileMap["msgtype"] = "gp"
        mapInterface := make(map[string]interface{})
        for key, value := range profileMap {
            strKey := fmt.Sprintf("%v", key)
            mapInterface[strKey] = value
        }
        return &mapInterface
    }
    return nil
}

func putDeviceId(deviceId *string, uid *string) {
    cdtMapPolicy := as.NewMapPolicyWithFlags(as.MapOrder.UNORDERED, as.MapWriteFlagsPartial)
    key, _ := as.NewKey("mesh", "data", *uid)
    dat := map[interface{}]interface{}{
        *deviceId: fmt.Sprintf("ws://%s:%s/mesh", mustHardwareAddr(), port),
    }
    asClient.Operate(WritePolicy, key,
        as.MapPutItemsOp(cdtMapPolicy, "ip", dat),
    );
}

func putPushToken(deviceId *string, uid *string, pushToken *string) {
    cdtMapPolicy := as.NewMapPolicyWithFlags(as.MapOrder.UNORDERED, as.MapWriteFlagsPartial)
    key, _ := as.NewKey("mesh", "data", *uid)
    dat := map[interface{}]interface{}{
        *deviceId: *pushToken,
    }
    asClient.Operate(WritePolicy, key,
        as.MapPutItemsOp(cdtMapPolicy, "pushtoken", dat),
    );
}

func deleteDeviceId(deviceId *string, uid *string) {
    key, _ := as.NewKey("mesh", "data", *uid)
    asClient.Operate(WritePolicy, key,
        as.MapRemoveByKeyOp("ip", *deviceId, as.MapReturnType.NONE),
    );
    delete(connClientMap, *deviceId)
}

func handleWsMessage(conn net.Conn) {
    fmt.Println("****************************")
    deviceId := ""
    isAuthenticated := false
    defer conn.Close()
    for {
        msg, _, err := wsutil.ReadClientData(conn)
        if err != nil {
            if err == io.EOF {
                return
            }
        }
        fmt.Println("****************************")

        var dat map[string]interface{}
        errJson := json.Unmarshal(msg, &dat)
        if errJson != nil {
            // handle error
        }
        
        fmt.Println(dat)
        if dat["msgtype"] == nil {
            continue
        }
        msgtype := dat["msgtype"].(string)
        deviceId = dat["deviceId"].(string)
        connClientMap[deviceId] = &conn
        switch msgtype {
            case "auth":
                idToken := dat["token"].(string)
                if idToken == "" {
                    fmt.Println("Invalid token", idToken)
                    return
                }
                fmt.Println("Token", idToken)

                client, err := firebaseApp.Auth(context.Background())
                if err != nil {
                    log.Fatalf("error getting Auth client: %v\n", err)
                    return
                }

                token, err := client.VerifyIDToken(context.Background(), idToken)
                if err != nil {
                    log.Fatalf("error verifying ID token: %v\n", err)
                    return
                }

                uid := dat["uid"].(string)
                putDeviceId(&deviceId, &uid)

                fmt.Println("***************deviceid", deviceId)
                isAuthenticated = true
                fmt.Println("++++++++++++++++++++++", token)
                defer deleteDeviceId(&deviceId, &uid)
            case "im":
                if !isAuthenticated {
                    fmt.Println("Not authenticated")
                    continue
                }

                from := dat["from"].(string)
                to := dat["to"].(string)

                insertMsgIntoAerospike(&dat, &msg)
                handleSendMessage(&dat, &from, &msg)
                handleSendMessage(&dat, &to, &msg)

            case "sp":
                if !isAuthenticated {
                    fmt.Println("Not authenticated")
                    continue
                }

                if err != nil && dat["partnerId"] == nil {
                    // Profile exists or no partnerId
                    continue
                }

                amap := map[interface{}]interface{}{
                    "email": dat["email"].(string),
                    "displayName": dat["displayName"].(string),
                    "avatarUrl": dat["avatarUrl"].(string),
                    "uid": dat["uid"].(string),
                }
                if dat["partnerId"] != nil {
                    amap["partnerId"] = dat["partnerId"].(string)
                }

                uid := dat["uid"].(string)
                profileMapPtr := putPartialUserMapAndGet(&amap, &uid)
                fmt.Println("<<<<<<<<<<<<<<<<<<<", *profileMapPtr)
                handleSendMessage(profileMapPtr, &uid, &msg)

                if dat["partnerId"] != nil {
                    pmap := map[interface{}]interface{}{
                        "partnerId": dat["uid"].(string),
                    }
                    uid := dat["partnerId"].(string)
                    profileMapPtr := putPartialUserMapAndGet(&pmap, &uid)
                    fmt.Println(">>>>>>>>>>>>>>", *profileMapPtr)
                    handleSendMessage(profileMapPtr, &uid, &msg)
                }
            case "pt":
                if !isAuthenticated {
                    fmt.Println("Not authenticated")
                    continue
                }

                uid := dat["uid"].(string)
                pushToken := dat["pushToken"].(string)
                putPushToken(&deviceId, &uid, &pushToken)

            case "iims":
                if !isAuthenticated {
                    fmt.Println("Not authenticated")
                    continue
                }

                connId := dat["connId"].(string)
                key, err := as.NewKey("mesh", "chats", connId)
                rec, err := asClient.Get(Policy, key, "common")
                if err != nil {
                    fmt.Println("+++++++++++++++++++++++", err)
                } else {
                    if rec != nil {
                        for key, value := range rec.Bins["common"].(map[interface{}]interface{}) {
                            if dat["timestamp"] == nil || (int64(key.(int)) > int64(dat["timestamp"].(float64))) {
                                err := wsutil.WriteServerMessage(conn, ws.OpText, value.([]byte))
                                fmt.Println("+++++++++++++++++++++++---")
                                if err != nil {
                                    fmt.Println("+++++++++++++++++++++++", err)
                                }
                            }
                        }
                    }
                }
        }
    }
}

func handleChatApi(w http.ResponseWriter, r *http.Request) {
    conn, _, _, err := ws.UpgradeHTTP(r, w)
    if err != nil {
        // handle error
        log.Fatalf("upgrade error: %v\n", err)
    }
    go handleWsMessage(conn)
}

func replyClient(dat *map[string]interface{}, msg *[]byte) {
    deviceId := (*dat)["toDeviceId"].(string)
    uid := (*dat)["toUid"].(string)
    pushToken := (*dat)["toPushToken"].(string)
    conn, ok := connClientMap[deviceId]
    if !ok {
        fmt.Println("dattttttttttttt4444444444", deviceId, conn, connClientMap)
        deleteDeviceId(&deviceId, &uid)
        sendPush(&pushToken, msg, dat)
    } else {
        fmt.Println("dattttttttttttt3333333333", deviceId, conn, connClientMap)
        err := wsutil.WriteServerMessage(*conn, ws.OpText, *msg)
        if err != nil {
            fmt.Println("errrroooooooooooooo", deviceId, conn)
            sendPush(&pushToken, msg, dat)
        }
    }
}

func handleMeshMessage(conn net.Conn) {
    defer conn.Close()
    for {
        msg, _, err := wsutil.ReadClientData(conn)
        if err != nil {
            if err == io.EOF {
                return
            }
        }

        var dat map[string]interface{}
        errJson := json.Unmarshal(msg, &dat)
        if errJson != nil {
            // handle error
        }
        
        msgtype := dat["msgtype"].(string)
        switch msgtype {
            case "im":
                replyClient(&dat, &msg)
            case "gp":
                replyClient(&dat, &msg)
        }
    }
}

func handleMeshApi(w http.ResponseWriter, r *http.Request) {
    conn, _, _, err := ws.UpgradeHTTP(r, w)
    if err != nil {
        // handle error
        log.Fatalf("upgrade error: %v\n", err)
    }
    fmt.Println("Mesh conn received")
    go handleMeshMessage(conn)
}

func handleGetChatsApi(w http.ResponseWriter, r *http.Request) {
    r.ParseForm()
    fmt.Println(r.Form)
    idToken := r.Form.Get("token")
    if idToken == "" {
        fmt.Println("Invalid token", idToken)
        w.WriteHeader(http.StatusUnauthorized)
        w.Write([]byte("401 - Unauthorized!"))
        return
    }
    fmt.Println("Token", idToken)

    client, err := firebaseApp.Auth(context.Background())
    if err != nil {
        log.Fatalf("error getting Auth client: %v\n", err)
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte("500 - Internal server error!"))
        return
    }

    _, err = client.VerifyIDToken(context.Background(), idToken)
    if err != nil {
        log.Fatalf("error verifying ID token: %v\n", err)
        w.WriteHeader(http.StatusUnauthorized)
        w.Write([]byte("401 - Unauthorized!"))
        return
    }

    connId := r.Form.Get("connId")
    ts := r.Form.Get("timestamp")
    timestamp, convErr := strconv.ParseInt(ts, 10, 64)
    key, err := as.NewKey("mesh", "chats", connId)
    rec, err := asClient.Get(Policy, key, "common")
    if err != nil {
        fmt.Println("+++++++++++++++++++++++", err)
        w.WriteHeader(http.StatusInternalServerError)
        w.Write([]byte("500 - Internal server error!"))
    } else {
        var ret []jMap
        if rec != nil {
            for key, value := range rec.Bins["common"].(map[interface{}]interface{}) {
                if convErr != nil || (int64(key.(int)) > timestamp) {
                    var msg map[string]interface{}
                    json.Unmarshal(value.([]byte), &msg)
                    ret = append(ret, msg)
                }
            }
        }
        msgTo, err := json.Marshal(ret)
        if err != nil {
            log.Fatalf("Internal error", err)
            w.WriteHeader(http.StatusInternalServerError)
            w.Write([]byte("500 - Internal server error!"))
            return
        } else {
            w.WriteHeader(http.StatusOK)
            w.Write(msgTo)
        }
    }
}

func main() {
    log.SetOutput(os.Stdout)
    fmt.Printf("hello, world\n")
    portPtr := flag.String("port", "8081", "Websocket port")
    flag.Parse()

    port = *portPtr

    opt := option.WithCredentialsFile("./vchat-7c774-firebase-adminsdk-np923-40843bc26d.json")

    app, err := firebase.NewApp(context.Background(), nil, opt)
    fmt.Println(reflect.TypeOf(app))
    if err != nil {
        log.Fatalf("error initializing app: %v\n", err)
    }
    firebaseApp = app

    hosts := []*as.Host {
        as.NewHost("139.59.94.255", 3000),
        // as.NewHost("another.host", 3000),
    }
    client, err := as.NewClientWithPolicyAndHost(nil, hosts...)
    if err != nil {
        log.Fatalf("error initializing aerospike: %v\n", err)
    }
    asClient = client

    ipAddr = mustHardwareAddr()

    go xmppInitiate()

    fmt.Println(port, fmt.Sprintf(":%s/mesh", port))

    http.HandleFunc("/chat", handleChatApi)
    http.HandleFunc("/getchats", handleGetChatsApi)
    http.HandleFunc("/mesh", handleMeshApi)
    http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
}