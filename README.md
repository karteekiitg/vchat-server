https://www.linode.com/docs/development/go/install-go-on-ubuntu/

go get -u github.com/json-iterator/go
go get -u github.com/gobwas/ws
go get -u firebase.google.com/go
go get -u github.com/aerospike/aerospike-client-go
go get -u github.com/scylladb/gocqlx
